# Localstack Starter Pack

This project bootstraps a dockerized version of Localstack that will accept the 
deployment of the bundled sample CDK app with an instance of a stack (`Cdklocal3Stack`), 
which contains an event bus and an SQS queue subscribed to a SNS topic. 

The `cdk.json` file tells the CDK Toolkit how to execute your app.

## Setup

### Workstation pre-reqs
First ensure the following tools are installed:   

* Docker   
* docker-compose   
* nodejs   
* pip/pip3   

### Install local deployment tooling

Install CDK npm tools separately to avoid deps issues (advice taken from official docs)   
Install a version of awslocal that is compatible with the version you are running (v1 vs v2).    
This project was tested using AWS CLI v1.   
``` 
$ sudo npm -g aws-cdk
$ sudo npm -g aws-cdk-local
$ pip install awslocal
```   

At the time of this writing this resulted in:   
```
$ which cdklocal
/usr/local/bin/cdklocal

$ cdklocal --version
2.11.0 (build f7148c5)

$ awslocal --version
aws-cli/1.22.24 Python/3.8.10 Linux/5.4.72-microsoft-standard-WSL2 botocore/1.23.46
```   

### Start LocalStack 

First configure some dummy credentials under your default profile.   
```
[default]
aws_access_key = test
aws_secret_Access_secret = test
```

Ensure that a region is configured by explicitly setting the Localstack region override variable. 
us-west-1 recommended for project compatibility.   
```
export DEFAULT_REGION=us-west-1
aws configure
```

Then run Localstack.   
```
$ docker-compose up [-d]   
```   

A successful installation will output a log similar to this:   
```
localstack_main | [2022-02-08 18:13:53 +0000] [21] [INFO] Running on http://0.0.0.0:37403 (CTRL + C to quit)
localstack_main | 2022-02-08T18:13:53:INFO:hypercorn.error: Running on http://0.0.0.0:37403 (CTRL + C to quit)
localstack_main | Starting mock CloudFormation service on http port 4566 ...
localstack_main | Starting mock CloudWatch service on http port 4566 ...
localstack_main | Starting mock DynamoDB service on http port 4566 ...
localstack_main | Starting mock DynamoDB Streams service on http port 4566 ...
localstack_main | Starting mock IAM service on http port 4566 ...
localstack_main | Starting mock STS service on http port 4566 ...
localstack_main | Starting mock Kinesis service on http port 4566 ...
localstack_main | Starting mock Lambda service on http port 4566 ...
localstack_main | Starting mock CloudWatch Logs service on http port 4566 ...
localstack_main | Starting mock Route53 service on http port 4566 ...
localstack_main | Starting mock S3 service on http port 4566 ...
localstack_main | Starting mock SNS service on http port 4566 ...
localstack_main | Starting mock SQS service on http port 4566 ...
localstack_main | Starting mock SSM service on http port 4566 ...
localstack_main | Starting mock Cloudwatch Events service on http port 4566 ...
localstack_main | Waiting for all LocalStack services to be ready
localstack_main | Ready.
localstack_main | 2022-02-08T18:14:01:INFO:bootstrap.py: Execution of "start_api_services" took 7972.47ms
```   

### Deploy resources

Build project   
`npm install`   

Register the CDK stack with the target environment. 
```
cdklocal bootstrap
```

Successful bootstrap logs will look similar to the log below. Note the fake account number!   
```
$ cdklocal bootstrap
 ⏳  Bootstrapping environment aws://000000000000/us-west-1...
Trusted accounts for deployment: (none)
Trusted accounts for lookup: (none)

 ✅  Environment aws://000000000000/us-west-1 bootstrapped.
```   

Finally, deploy the stack and verify resources using the `awslocal` wrapper.   

```
cdklocal deploy

awslocal cloudformation list-stacks 

        {
            "StackId": "arn:aws:cloudformation:us-west-1:000000000000:stack/Cdklocal3Stack/7f97ca80",
            "StackName": "Cdklocal3Stack",
            "CreationTime": "2022-02-08T18:52:03.094Z",
            "StackStatus": "CREATE_COMPLETE",
            "StackStatusReason": "Deployment succeeded"
        }
```   

## Commands Reference

 * `npm run build`   compile typescript to js
 * `npm run watch`   watch for changes and compile
 * `npm run test`    perform the jest unit tests
 * `cdk bootstrap`   register this CDK context with your default AWS account/region
 * `cdk deploy`      deploy this stack to your default AWS account/region
 * `cdk diff`        compare deployed stack with current state
 * `cdk synth`       emits the synthesized CloudFormation template
