#!/usr/bin/env node
import * as cdk from '@aws-cdk/core';
import { Cdklocal3Stack } from '../lib/cdklocal3-stack';

const app = new cdk.App();
new Cdklocal3Stack(app, 'Cdklocal3Stack');
