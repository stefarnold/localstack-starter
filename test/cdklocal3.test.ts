import { expect as expectCDK, haveResource } from '@aws-cdk/assert';
import * as cdk from '@aws-cdk/core';
import * as Cdklocal3 from '../lib/cdklocal3-stack';

test('SQS Queue Created', () => {
    const app = new cdk.App();
    // WHEN
    const stack = new Cdklocal3.Cdklocal3Stack(app, 'MyTestStack');
    // THEN
    expectCDK(stack).to(haveResource("AWS::SQS::Queue",{
      VisibilityTimeout: 300
    }));
});

test('SNS Topic Created', () => {
  const app = new cdk.App();
  // WHEN
  const stack = new Cdklocal3.Cdklocal3Stack(app, 'MyTestStack');
  // THEN
  expectCDK(stack).to(haveResource("AWS::SNS::Topic"));
});

test('Non-default Event Bus Created', () => {
  const app = new cdk.App();
  // WHEN
  const stack = new Cdklocal3.Cdklocal3Stack(app, 'MyTestStack');
  // THEN
  expectCDK(stack).to(haveResource("AWS::Events::EventBus"));
});
